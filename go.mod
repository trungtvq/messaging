module gitlab.com/trungtvq/messaging

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/opentracing/opentracing-go v1.1.0
	github.com/spf13/cobra v0.0.7
	github.com/spf13/viper v1.6.2
	github.com/trungtvq/go-utils v1.2.6
	go.uber.org/zap v1.12.0
)
