package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/trungtvq/messaging/config"
	"gitlab.com/trungtvq/messaging/monitor"
	"go.uber.org/zap"
)

var cfgFile string

func initService() {
	config.InitConfig(cfgFile)
	monitor.InitLogger()
	monitor.InitTracer()
	monitor.Logger.Bg().Info("initService SUCCESS")
}

func init() {
	cobra.OnInitialize(initService)
	RootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file as toml (default is config.dev.toml)")
}

// RootCmd Process message from Kafka, gRPC, RestAPI
var RootCmd = &cobra.Command{
	Use:   "Messaging",
	Short: "Many of function can be used in compile with shell script way",
}

// Execute ...
func Execute() {
	RootCmd.AddCommand(versionCmd)
	if err := RootCmd.Execute(); err != nil {
		fmt.Println("Start program failed", zap.Error(err))
		os.Exit(-1)
	}
}
